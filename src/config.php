<?php

return [
  'title'        => 'Dashboard',
  'credits'      => 'Odot Media LLC',
  'defaultRole'  => 'registered',
  'registration' => false,
  'activations'  => false,
  'routes'       => true,
];
